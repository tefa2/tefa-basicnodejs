const transportasi = require("./vehicle")

function main() {
    let transport = new transportasi("Bejo", "Mobil", 4, "N 1945 CI")
    console.log("Selamat datang di Travel", transport.nama)
    console.log("================================")
    console.log("Transportasi yang dipakai", transport.jenis)
    console.log("dengan plat nomor", transport.plat)
    console.log("Jumlah penumpang =", transport.penumpang, "orang")

    transport.asal_tujuan("Malang", "Surabaya")
    transport.jarak(80)
    console.log("================================")
    transport.keterangan()
}

main()