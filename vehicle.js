class Kendaraan {
    constructor(nama_rental, jenis_kendaraan, penumpang, plat) {
        this.nama = nama_rental
        this.jenis = jenis_kendaraan
        this.penumpang = penumpang
        this.plat = plat
    }
    
    asal_tujuan(asal, tujuan) {
        console.log("Rute", asal, "-", tujuan)
    }

    jarak(jarak) {
        console.log("dengan jarak", jarak, "Km")
    }

    keterangan() {
        console.log("Hati - Hati di jalan😁")
    }
}

module.exports = Kendaraan